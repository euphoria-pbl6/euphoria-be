package com.example.homeservice.payload.dto;

public interface BookedDateDTO {
    String getBookedDate();
}
