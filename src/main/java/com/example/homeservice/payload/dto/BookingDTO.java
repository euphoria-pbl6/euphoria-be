package com.example.homeservice.payload.dto;


import com.example.homeservice.domain.enums.BookingStatus;
import com.example.homeservice.domain.enums.RatingStatus;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;

public interface BookingDTO {
    String getId();
    Timestamp getCancelDate();
    LocalDate getCheckInDate();
    LocalDate getCheckOutDate();
    Timestamp getCreatedAt();
    Timestamp getUpdatedAt();
    String getHomeId();
    String getUserId();
    String getTransactionId();
    boolean getIsRefund();
    BigDecimal getPriceForStay();
    BigDecimal getPricePerDay();
    BookingStatus getStatus();

    RatingStatus getRating();

  int getNumOfAdults();

  int getNumOfBabies();

  int getNumOfChildren();
}
