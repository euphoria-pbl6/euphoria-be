package com.example.homeservice.payload.response.booking;

import com.euphoria.common.domain.model.UserInfo;
import com.example.homeservice.domain.enums.BookingStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class BookingResponse {
    private UUID id;
    private UserInfo userInfo;
    private Object home;
    private int numOfAdults;
    private int numOfChildren;
    private int numOfBabies;
    private int numOfPets;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private Timestamp createdAt;
    private Timestamp cancelDate;
    private Timestamp updatedAt;
    private BigDecimal pricePerDay;
    private boolean isRefund;
    private BigDecimal priceForStay;
    @Enumerated(EnumType.STRING)
    private BookingStatus status;
    private TransactionsResponse transaction;
    private String rating;
}
