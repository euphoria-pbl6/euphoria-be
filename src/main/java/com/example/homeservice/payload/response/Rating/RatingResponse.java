package com.example.homeservice.payload.response.Rating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RatingResponse {
    private Double cleanliness;

    private Double accuracy;

    private Double communication;

    private Double checkIn;

    private Double location;

    private Double value;

    private List<ReviewResponse> reviewResponses;
}
