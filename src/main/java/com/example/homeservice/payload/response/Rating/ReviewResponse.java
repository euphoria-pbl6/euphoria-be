package com.example.homeservice.payload.response.Rating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReviewResponse {
    private UUID idUser;
    private String review;
    private LocalDate dateCreate;
    private UUID idHome;
}
