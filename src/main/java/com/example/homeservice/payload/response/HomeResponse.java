package com.example.homeservice.payload.response;

import com.example.homeservice.domain.model.Home;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomeResponse {
    private UUID id;
    private String name;
    private String address;
    private List<String> files;
    private double price;

    private LocalDate startAt;

    private LocalDate endAt;

    public HomeResponse(Home home) {
        this.id = home.getId();
        this.name = home.getName();
        this.address = home.getAddress();
        this.files = home.getFiles();
        this.price = home.getPrice();
        this.startAt = home.getStartAt();
        this.endAt = home.getEndAt();
    }
}
