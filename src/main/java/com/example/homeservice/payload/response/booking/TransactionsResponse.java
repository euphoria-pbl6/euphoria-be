package com.example.homeservice.payload.response.booking;

import ch.qos.logback.core.joran.action.TimestampAction;
import com.example.homeservice.domain.enums.TransactionStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TransactionsResponse {
    private BigDecimal amount;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Timestamp transferOn;
    private TransactionStatus status;
    private String bankTranNo;
    private String txtRef;
    private String paymentUrl;
    private Date timeOver;
}
