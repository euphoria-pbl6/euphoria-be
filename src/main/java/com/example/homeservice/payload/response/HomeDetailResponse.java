package com.example.homeservice.payload.response;

import com.example.homeservice.domain.model.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HomeDetailResponse {
    private UUID id;
    private String name;
    private String description;
    private String address;
    private int numOfGuests;
    private int numOfPets;
    private int numOfBabies;
    private int numOfBeds;
    private int numOfBedrooms;
    private int numOfBathrooms;
    private double price;
    private double priceService;
    private double priceForPet;
    private Category category;
    private List<Bathroom> bathroom;
    private List<BedroomAndLaundry> bedroomAndLaundry;
    private List<Entertainment> entertainment;
    private List<Family> family;
    private List<HeatingAndCooling> heatingAndCooling;
    private List<HomeSafety> homeSafety;
    private List<InternetAndOffice> internetAndOffice;
    private List<KitchenAndDining> kitchenAndDining;
    private List<LocationFeatures> locationFeature;
    private List<Outdoor> outdoor;
    private List<ParkingAndFacilities> parkingAndFacilities;
    private List<Services> services;
    private List<String> files;

    private Object user;

    private LocalDate startAt;

    private LocalDate endAt;

    private Date dateCreate;

    private String status;
}
