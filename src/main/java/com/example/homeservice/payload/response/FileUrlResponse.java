package com.example.homeservice.payload.response;

public class FileUrlResponse {
    private String status;

    private String data;

    public FileUrlResponse(String status, String data) {
        this.status = status;
        this.data = data;
    }

    public FileUrlResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
