package com.example.homeservice.payload.request.rating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RatingRequest {
    private UUID idBooking;

    private Double cleanliness;

    private Double accuracy;

    private Double communication;

    private Double checkIn;

    private Double location;

    private Double value;

    private String review;
}
