package com.example.homeservice.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomeRequest {
    private String name;
    private String description;
    private String address;
    private int numOfGuests;
    private int numOfBeds;
    private int numOfBedrooms;
    private int numOfBathrooms;
    private int numOfPets;
    private int numOfBabies;
    private double priceForPet;
    private double price;
    private double priceService;
    private int category;
    private List<String> bathroom;
    private List<String> bedroomAndLaundry;
    private List<String> entertainment;
    private List<String> family;
    private List<String> heatingAndCooling;
    private List<String> homeSafety;
    private List<String> internetAndOffice;
    private List<String> kitchenAndDining;
    private List<String> locationFeature;
    private List<String> outdoor;
    private List<String> parkingAndFacilities;
    private List<String> services;
    private LocalDate startAt;
    private LocalDate endAt;
    private List<String> files;
}
