package com.example.homeservice.common;

import com.example.homeservice.common.exception.BadRequestException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.regex.Pattern;


public class CommonFunction {
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static Timestamp toTimestamp(String inputDate) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputDate);
        return new Timestamp(date.getTime());
    }
    public static String toEngChar(String vietnamStr){
        vietnamStr = vietnamStr.toUpperCase();
        String finalWord = vietnamStr.replaceAll("A|Á|Ả|À|Ã|Ạ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ|Ă|Ẳ|Ắ|Ằ|Ẵ|Ặ", "A");
        finalWord = finalWord.replaceAll("E|É|È|Ẽ|Ẻ|Ẹ|Ê|Ế|Ề|Ễ|Ể|Ệ", "E");
        finalWord = finalWord.replaceAll("I|Í|Ì|Ĩ|Ị|Ỉ", "I");
        finalWord = finalWord.replaceAll("O|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ổ|Ố|Ồ|Ỗ|Ộ|Ơ|Ớ|Ở|Ờ|Ỡ|Ợ", "O");
        finalWord = finalWord.replaceAll("U|Ú|Ủ|Ù|Ũ|Ụ|Ư|Ứ|Ừ|Ữ|Ủ|Ự", "U");
        finalWord = finalWord.replaceAll("Y|Ý|Ỳ|Ỹ|Ỵ|Ỷ", "Y");
        finalWord = finalWord.replaceAll("Đ", "D");
        return finalWord;
    }
    public static Map<String,Object> getDataFromClientService(Map<String,Object> response){
        if(response.containsKey("error")){
            Map<String,Object> error = (Map<String, Object>) response.get("error");
            throw new BadRequestException(error.get("error").toString());
        }
        return (Map<String, Object>) response.get("data");
    }
    public static String hmacSha512(String key, String message)
    {
        Mac sha512Hmac;
        String result;

        try {
            final byte[] byteKey = key.getBytes(StandardCharsets.UTF_8);
            sha512Hmac = Mac.getInstance("HmacSHA512");
            SecretKeySpec keySpec = new SecretKeySpec(byteKey, "HmacSHA512");
            sha512Hmac.init(keySpec);
            byte[] macData = sha512Hmac.doFinal(message.getBytes(StandardCharsets.UTF_8));

            // Can either base64 encode or put it right into hex
            result = Base64.getEncoder().encodeToString(macData);
            result = bytesToHex(macData);
            return result;
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        } finally {
            // Put any cleanup here
            System.out.println("Done");
        }
        return "";
    }
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
