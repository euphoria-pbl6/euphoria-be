package com.example.homeservice.common.constant;

public final class StatusMessage {
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
}
