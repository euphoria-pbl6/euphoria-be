package com.example.homeservice.common.constant;

public class ErrorMessage {
    public static final String HOME_NOT_EXISTS = "home_not_exists";

    public static final String FILE_NOT_FORMAT = "file_not_format";

    public static final String HOME_CONFIRMED = "home_confirmed";
    public static final String CHECK_IN_CANT_EQUAL_CHECK_OUT = "check_in_cant_equal_check_out";
    public static final String TRANSACTION_NOT_FOUND = "transaction_not_found";
    public static String CHECK_IN_CANT_AFTER_CHECK_OUT = "check_in_cant_after_check_out";
    public static String BOOKING_DATE_NOT_AVAILABLE = "booking_date_not_available";
    public static String ACCESS_DENIED = "access_denied";
    public static String BOOKING_STATUS_NOT_MATCH = "booking_status_not_match";
    public static String TOKEN_NOT_VALID = "token_is_not_valid";
    public static String BOOKING_NOT_FOUND_OR_ACCESS_DENIED = "booking_not_found_or_access_denied";
    public static String FAIL_TO_ACCEPT_BOOKING = "fail_to_accept_booking";
    public static String FAIL_TO_REJECT_BOOKING = "fail_to_reject_booking";
    public static String FAIL_TO_CANCEL_BOOKING = "fail_to_cancel_booking";
    public static String FILE_CANT_LARGE_THAN_5_MB = "file_cant_large_than_5_mb";
    public static String FILES_REQUEST_TOO_LARGE = "files_request_too_large";

    public static String HOME_NOT_EDIT = "home_not_edit";

    public static String CATEGORY_NOT_EXISTS = "category_not_exists";

    public static String NOT_RATED = "not_rated";

    public static String RATING_NOT_EXISTS = "rating_not_exists";
}
