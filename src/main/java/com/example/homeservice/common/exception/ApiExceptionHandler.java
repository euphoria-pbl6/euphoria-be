package com.example.homeservice.common.exception;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.common.constant.ErrorMessage;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import io.sentry.Sentry;
import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ResponseCustom> forbiddenException(
            ForbiddenException ex) {
        return new ResponseEntity(
                ResponseCustom.errorResponse(ex.getMessage()), HttpStatus.FORBIDDEN);
    }
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ResponseCustom> notFoundException(
            NotFoundException ex, HttpServletRequest request) {
        Sentry.captureException(ex);
        return new ResponseEntity(
                ResponseCustom.errorResponse(ex.getMessage()), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ResponseCustom> notFoundException(
            ResourceNotFoundException ex, HttpServletRequest request) {
        Sentry.captureException(ex);
        return new ResponseEntity(
                ResponseCustom.errorResponse(ex.getMessage()), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(FileSizeLimitExceededException.class)
    public ResponseEntity<ResponseCustom> fileSizeLimitException(
            FileSizeLimitExceededException ex, HttpServletRequest request) {
        Sentry.captureException(ex);
        Sentry.captureMessage(String.format("{url: %s, exception:%s}",request.getServletPath(),ex.getMessage()));
        return new ResponseEntity(
                ResponseCustom.errorResponse(ErrorMessage.FILE_CANT_LARGE_THAN_5_MB), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(SizeLimitExceededException.class)
    public ResponseEntity<ResponseCustom> sizeLimitExceededException(
            SizeLimitExceededException ex, HttpServletRequest request) {
        Sentry.captureMessage(String.format("{url: %s, exception:%s}",request.getServletPath(),ex.getMessage()));
        Sentry.captureException(ex);
        return new ResponseEntity(
                ResponseCustom.errorResponse(ErrorMessage.FILES_REQUEST_TOO_LARGE), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ResponseCustom> badRequestException(
            BadRequestException ex, HttpServletRequest request) {
        Sentry.captureMessage(String.format("{url: %s, exception:%s",request.getServletPath(),ex.getMessage()));
        return new ResponseEntity(
                ResponseCustom.errorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

}
