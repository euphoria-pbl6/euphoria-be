package com.example.homeservice.domain.model;

import brave.internal.Nullable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="category")
public class Category {
    @Id
    @GeneratedValue
    private int id;

    @Column
    private String title;

    @Column
    @Nullable
    private String description;

    @Column
    private String icon;
}
