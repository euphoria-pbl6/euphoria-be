package com.example.homeservice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="home_safety")
public class HomeSafety {
    @Id
    @GeneratedValue
    private int id;

    @Column
    private String title;

    @Column
    private String icon;
}
