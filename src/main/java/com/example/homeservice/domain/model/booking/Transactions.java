package com.example.homeservice.domain.model.booking;

import com.example.homeservice.domain.enums.TransactionStatus;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Entity
@AllArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
@Data
public class Transactions {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    private BigDecimal amount;
    private Timestamp transferOn;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;
    private String bankTranNo;
    private String txtRef;
    private Date timeOver;
    @Column(length = 1000)
    private String paymentURL;
}
