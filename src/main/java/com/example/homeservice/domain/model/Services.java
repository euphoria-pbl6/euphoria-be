package com.example.homeservice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="services")
public class Services {
    @Id
    @GeneratedValue
    private int id;

    @Column
    private String title;

    @Column
    private String icon;
}
