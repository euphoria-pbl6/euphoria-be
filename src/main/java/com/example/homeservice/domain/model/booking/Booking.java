package com.example.homeservice.domain.model.booking;

import com.example.homeservice.domain.enums.BookingStatus;
import com.example.homeservice.domain.enums.RatingStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;


@Entity
@AllArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)

public class Booking {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    private UUID userId;
    private UUID homeId;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private Timestamp createdAt;
    private Timestamp cancelDate;
    private Timestamp updatedAt;
    private BigDecimal pricePerDay;
    private String reason;
    private int numOfPets;
    @Column
    private int numOfAdults;
    @Column
    private int numOfChildren;
    @Column
    private int numOfBabies;
    private boolean isRefund;
    private BigDecimal priceForStay;
    @Enumerated(EnumType.STRING)
    private BookingStatus status;
    @OneToOne
    @JoinColumn(name = "transactions_id")
    private Transactions transactions;
    @Enumerated(EnumType.STRING)
    private RatingStatus rating;
}
