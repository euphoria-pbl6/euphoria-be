package com.example.homeservice.domain.model;

import com.example.homeservice.payload.request.HomeRequest;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="home")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class Home{
    @Id
    @GeneratedValue
    private UUID id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String idUser;

    @Column
    private String address;

    @Column
    private Date dateCreate;
    @Column
    private int numOfGuests;

    @Column
    private int numOfBeds;

    @Column
    private int numOfBedrooms;

    @Column
    private int numOfBathrooms;

    @Column
    private double price;

    @Column
    private LocalDate startAt;

    @Column
    private LocalDate endAt;
    @Column
    private int numOfPets;
    @Column
    private int numOfBabies;
    @Column
    private double priceForPet;
    @Column
    private double priceService;

    @Type(type = "list-array")
    @Column(
            name = "files",
            columnDefinition = "text[]"
    )
    private List<String> files;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;
    @Type(type = "list-array")
    @Column(
            name = "bathroom",
            columnDefinition = "text[]"
    )
    private List<String> bathroom;

    @Type(type = "list-array")
    @Column(
            name = "bedroom_laundry",
            columnDefinition = "text[]"
    )
    private List<String> bedroomAndLaundry;

    @Type(type = "list-array")
    @Column(
            name = "entertainment",
            columnDefinition = "text[]"
    )
    private List<String> entertainment;

    @Type(type = "list-array")
    @Column(
            name = "family",
            columnDefinition = "text[]"
    )
    private List<String> family;

    @Type(type = "list-array")
    @Column(
            name = "heating_cooling",
            columnDefinition = "text[]"
    )
    private List<String> heatingAndCooling;

    @Type(type = "list-array")
    @Column(
            name = "homeSafety",
            columnDefinition = "text[]"
    )
    private List<String> homeSafety;

    @Type(type = "list-array")
    @Column(
            name = "internet_office",
            columnDefinition = "text[]"
    )
    private List<String> internetAndOffice;

    @Type(type = "list-array")
    @Column(
            name = "kitchen_dining",
            columnDefinition = "text[]"
    )
    private List<String> kitchenAndDining;

    @Type(type = "list-array")
    @Column(
            name = "locationFeature",
            columnDefinition = "text[]"
    )
    private List<String> locationFeature;

    @Type(type = "list-array")
    @Column(
            name = "outdoor",
            columnDefinition = "text[]"
    )
    private List<String> outdoor;

    @Type(type = "list-array")
    @Column(
            name = "parking_facility",
            columnDefinition = "text[]"
    )
    private List<String> parkingAndFacilities;

    @Type(type = "list-array")
    @Column(
            name = "services",
            columnDefinition = "text[]"
    )
    private List<String> services;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    private Status status;

    public Home(HomeRequest homeRequest) {
        this.name = homeRequest.getName();
        this.description = homeRequest.getDescription();
        this.address = homeRequest.getAddress();
        this.numOfGuests = homeRequest.getNumOfGuests();
        this.numOfBeds = homeRequest.getNumOfBeds();
        this.numOfBedrooms = homeRequest.getNumOfBedrooms();
        this.numOfBathrooms = homeRequest.getNumOfBathrooms();
        this.numOfPets = homeRequest.getNumOfPets();
        this.numOfBabies = homeRequest.getNumOfBabies();
        this.price = homeRequest.getPrice();
        this.priceService = homeRequest.getPriceService();
        this.priceForPet = homeRequest.getPriceForPet();
        this.bathroom = homeRequest.getBathroom();
        this.bedroomAndLaundry = homeRequest.getBedroomAndLaundry();
        this.entertainment = homeRequest.getEntertainment();
        this.family = homeRequest.getFamily();
        this.heatingAndCooling = homeRequest.getHeatingAndCooling();
        this.homeSafety = homeRequest.getHomeSafety();
        this.internetAndOffice = homeRequest.getInternetAndOffice();
        this.kitchenAndDining = homeRequest.getKitchenAndDining();
        this.locationFeature = homeRequest.getLocationFeature();
        this.outdoor = homeRequest.getOutdoor();
        this.parkingAndFacilities = homeRequest.getParkingAndFacilities();
        this.services = homeRequest.getServices();
        this.startAt = homeRequest.getStartAt();
        this.endAt = homeRequest.getEndAt();
        this.files = homeRequest.getFiles();
    }
}
