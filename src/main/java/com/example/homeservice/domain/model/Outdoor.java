package com.example.homeservice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="ourdoor")
public class Outdoor {
    @Id
    @GeneratedValue
    private int id;

    @Column
    private String title;

    @Column
    private String icon;
}
