package com.example.homeservice.domain.model;

import com.example.homeservice.domain.model.booking.Booking;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "rating")
public class Rating {
    @Id
    @GeneratedValue
    private UUID id;

    @Column
    private Double cleanliness;

    @Column
    private Double accuracy;

    @Column
    private Double communication;

    @Column
    private Double checkIn;

    @Column
    private Double location;

    @Column
    private Double value;

    @Column
    private String review;

    @Column
    private LocalDate dateCreate;

    @Column
    private UUID idUser;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "booking_id", referencedColumnName = "id")
    private Booking booking;

    public Rating(Double cleanliness, Double accuracy, Double communication, Double checkIn, Double location, Double value, String review, LocalDate dateCreate, UUID idUser, Booking booking) {
        this.cleanliness = cleanliness;
        this.accuracy = accuracy;
        this.communication = communication;
        this.checkIn = checkIn;
        this.location = location;
        this.value = value;
        this.review = review;
        this.dateCreate = dateCreate;
        this.idUser = idUser;
        this.booking = booking;
    }
}
