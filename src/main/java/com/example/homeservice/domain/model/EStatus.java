package com.example.homeservice.domain.model;

public enum EStatus {
    WAITING_APPROVE,
    APPROVED,
    DISAPPROVED
}
