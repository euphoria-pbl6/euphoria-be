package com.example.homeservice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "status")
public class Status {
	@Id
	@GeneratedValue
	private int id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private EStatus name;

	public Status(EStatus name) {
		this.name = name;
	}
}