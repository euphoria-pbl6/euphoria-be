package com.example.homeservice.domain.enums;

public enum RatingStatus {
    RATED, NOT_YET_RATED
}
