package com.example.homeservice.domain.enums;

public enum BookingStatus {
    CANCEL,WAITING,ACCEPTED,REJECT,PAID
}
