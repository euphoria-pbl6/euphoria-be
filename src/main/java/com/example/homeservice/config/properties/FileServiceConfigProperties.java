package com.example.homeservice.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "euphoria")
public class FileServiceConfigProperties {
    @NotBlank private String projectId;

    @NotBlank private String bucketName;

    @NotBlank
    private String credentialPath;
    @NotBlank private String urlFirebase;

}
