package com.example.homeservice.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "vnpay")
public class VNPayProperties {
    @NotBlank private String hashSecret;
    @NotBlank private String tmnCode;
    @NotBlank private String paymentUrl;
    @NotBlank private String returnUrl;
}
