package com.example.homeservice.config;

import com.euphoria.common.ResponseCustom;
import com.euphoria.common.domain.model.UserPrincipal;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.exception.ForbiddenException;
import com.example.homeservice.service.client.AuthClientService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class JWTVerifierFilter extends OncePerRequestFilter {
  @Autowired
  private AuthClientService authClientService;

  @Override
  protected void doFilterInternal(
          HttpServletRequest httpServletRequest,
          HttpServletResponse httpServletResponse,
          FilterChain filterChain)
          throws ServletException, IOException {
    String authToken = httpServletRequest.getHeader("Authorization");
    try {
      if (authToken != null && authToken.startsWith("Bearer ")) {
        UserPrincipal userPrincipal = authClientService.validate(authToken);
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = Collections.singletonList(new SimpleGrantedAuthority(userPrincipal.getRoles()));
        UsernamePasswordAuthenticationToken authentication =
                new UsernamePasswordAuthenticationToken(userPrincipal, null, simpleGrantedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
      }
      filterChain.doFilter(httpServletRequest, httpServletResponse);

    } catch (ForbiddenException ex) {
      httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
      httpServletResponse.setContentType("application/json");
      httpServletResponse.setCharacterEncoding("UTF-8");

      httpServletResponse
              .getWriter()
              .write(new ObjectMapper().writeValueAsString(ResponseCustom.errorResponse(ex.getMessage())));
    }

  }



}
