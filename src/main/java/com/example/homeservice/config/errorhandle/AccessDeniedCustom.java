package com.example.homeservice.config.errorhandle;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.common.constant.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AccessDeniedCustom implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response
                .getWriter()
                .write(new ObjectMapper().writeValueAsString(ResponseCustom.errorResponse(ErrorMessage.ACCESS_DENIED)));
        response.setStatus(403);
    }
}
