package com.example.homeservice.config.errorhandle;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.common.constant.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response
                .getWriter()
                .write(new ObjectMapper().writeValueAsString(ResponseCustom.errorResponse(ErrorMessage.TOKEN_NOT_VALID)));
        response.setStatus(401);
    }
}
