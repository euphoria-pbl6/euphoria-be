package com.example.homeservice.config;

import io.sentry.spring.EnableSentry;
import org.springframework.context.annotation.Configuration;

@EnableSentry(dsn = "https://02e9586fff9243968c4222e224de82cc@o4504282855833600.ingest.sentry.io/4504282857996288")
@Configuration
public class SentryConfiguration {
}