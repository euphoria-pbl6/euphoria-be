package com.example.homeservice.config;

import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.exception.BadRequestException;
import com.example.homeservice.common.exception.ForbiddenException;

import com.example.homeservice.common.exception.NotFoundException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.CharStreams;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;


@Component
public class FeignErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        String message = null;
        Reader reader = null;
        try {
            reader = response.body().asReader();
            String result = CharStreams.toString(reader);
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            Map<String, Object> body = mapper.readValue(result, HashMap.class);
            message = body.get("error").toString();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {

                if (reader != null)
                    reader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        switch (response.status()){
            case 400:
              return new BadRequestException(message);
            case 403:
            {
                return new ForbiddenException(message);
            }
            case 404:
                return new NotFoundException(message);
            default:
                return new Exception(response.reason());
        }
    }

}
