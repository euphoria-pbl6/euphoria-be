package com.example.homeservice.controller;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.payload.request.payment.PaymentResultRequest;
import com.example.homeservice.service.payment.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.ParseException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/payments")
public class PaymentController {
  private final PaymentService paymentService;

  @GetMapping
  public ResponseEntity<ResponseCustom> paymentResultRoute(
      @RequestParam(value = "vnp_Amount", defaultValue = "0") long amount,
      @RequestParam(value = "vnp_BankTranNo", defaultValue = "") String bankTranNo,
      @RequestParam(value = "vnp_TransactionNo", defaultValue = "") String transactionNo,
      @RequestParam(value = "vnp_CardType", defaultValue = "") String cardType,
      @RequestParam(value = "vnp_PayDate", defaultValue = "") String payDate,
      @RequestParam(value = "vnp_ResponseCode", defaultValue = "") String responseCode,
      @RequestParam(value = "vnp_TransactionStatus", defaultValue = "") String transactionStatus,
      @RequestParam(value = "vnp_TxnRef", defaultValue = "") String txnRef)
      throws ParseException {
    System.out.println("payment trigger");
    PaymentResultRequest request = new PaymentResultRequest();
    request.setAmount(BigDecimal.valueOf(amount));
    request.setTransactionNo(transactionNo);
    request.setTransferOn(payDate);
    request.setResponseCode(responseCode);
    request.setTxtRef(txnRef);
    paymentService.getPayment(request);
    return ResponseEntity.ok(ResponseCustom.successResponse(null));
  }
}
