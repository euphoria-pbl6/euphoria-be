package com.example.homeservice.controller;

import com.example.homeservice.service.HomeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/admin")
public class AdminController {
    private final HomeService homeService;

    @GetMapping("/getHomesWaitingApprove")
    public ResponseEntity getHomesWaitingApprove(@RequestParam(value = "page", defaultValue = "1") int page,
                                                 @RequestParam(value = "paging", defaultValue = "10") int paging){
        return homeService.getHomeWaitingApprove(page, paging);
    }

    @PostMapping("/confirmHome/{idHome}")
    public ResponseEntity confirmHome(@PathVariable UUID idHome,
                                      @RequestParam(value = "status", defaultValue = "APPROVED") String status){
        return homeService.confirmHome(idHome, status);
    }
}
