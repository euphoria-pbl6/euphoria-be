package com.example.homeservice.controller;

import com.euphoria.common.ResponseCustom;
import com.euphoria.common.domain.model.UserPrincipal;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.payload.request.HomeRequest;
import com.example.homeservice.service.HomeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;


@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/home")
public class HomeController {
    private final HomeService homeService;

    @GetMapping("/")
    public ResponseEntity getHome(@RequestParam(value = "category", defaultValue = "0") int category,
                                  @RequestParam(value = "page", defaultValue = "1") int page,
                                  @RequestParam(value = "paging", defaultValue = "10") int paging,
                                  @RequestParam(value = "order", defaultValue = "desc") String orderBy){
        return homeService.getHome(category, page, paging,orderBy);
    }

//    @HystrixCommand(fallbackMethod = "fallbackHome")
    @GetMapping("/{id}")
    public ResponseEntity getHome(@PathVariable UUID id){
        return homeService.getHome(id);
    }

    // a fallback method to be called if failure happened
    public ResponseEntity fallbackHome(UUID id, Throwable hystrixCommand) {
        return new ResponseEntity(ResponseCustom.errorResponse(ErrorMessage.HOME_NOT_EXISTS), HttpStatus.NOT_FOUND);
    }

    @PostMapping
    @PreAuthorize("hasRole('OWNER')")
    public ResponseEntity createHome(@AuthenticationPrincipal UserPrincipal userPrincipal,HttpServletRequest request,
                                     @RequestBody HomeRequest homeRequest){
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(homeService.createHome(userPrincipal.getId().toString(), homeRequest), null));
    }

    @GetMapping("/getHomeByIdUser")
    public ResponseEntity getHomeByIdUser(@RequestParam(value = "idUser",defaultValue = "00000000-0000-0000-0000-000000000000") String idUser,
                                          @RequestParam(value = "page", defaultValue = "1") int page,
                                          @RequestParam(value = "paging", defaultValue = "10") int paging,
                                          @RequestParam(value = "order", defaultValue = "desc") String orderBy){
        return homeService.getHomeByIdUser(idUser, page, paging,orderBy);
    }
    @GetMapping("/getCategories")
    public ResponseEntity getCategories(){
        return homeService.getCategories();
    }

    @GetMapping("/getBathroom")
    public ResponseEntity getBathRoom(){
        return homeService.getBathroom();
    }

    @GetMapping("/getBedroomAndLaundry")
    public ResponseEntity getBedroomAndLaundry(){
        return homeService.getBedroomAndLaundry();
    }

    @GetMapping("/getEntertainment")
    public ResponseEntity getEntertainment(){
        return homeService.getEntertainment();
    }

    @GetMapping("/getFamily")
    public ResponseEntity getFamily(){
        return homeService.getFamily();
    }

    @GetMapping("/getheatingAndCooling")
    public ResponseEntity getheatingAndCooling(){
        return homeService.getHeatingAndCooling();
    }

    @GetMapping("/getHomeSafety")
    public ResponseEntity getHomeSafety(){
        return homeService.getHomeSafety();
    }

    @GetMapping("/getInternetAndOffice")
    public ResponseEntity getInternetAndOffice(){
        return homeService.getInternetAndOffice();
    }

    @GetMapping("/getKitchenAndDining")
    public ResponseEntity getKitchenAndDining(){
        return homeService.getKitchenAndDining();
    }

    @GetMapping("/getLocationFeature")
    public ResponseEntity getLocationFeature(){
        return homeService.getLocationFeature();
    }

    @GetMapping("/getOutdoor")
    public ResponseEntity getOutdoor(){
        return homeService.getOutdoor();
    }

    @GetMapping("/getParkingAndFacilities")
    public ResponseEntity getParkingAndFacilities(){
        return homeService.getParkingAndFacilities();
    }

    @GetMapping("/getServices")
    public ResponseEntity getServices(){
        return homeService.getServices();
    }

    @PreAuthorize("hasRole('OWNER')")
    @PutMapping("/{idHome}")
    public ResponseEntity editHome(@PathVariable UUID idHome, @RequestBody HomeRequest homeRequest){
        return homeService.editHome(idHome, homeRequest);
    }
}
