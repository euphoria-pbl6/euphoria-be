package com.example.homeservice.controller;

import com.euphoria.common.domain.model.UserPrincipal;
import com.example.homeservice.payload.request.booking.BookingRequest;
import com.example.homeservice.payload.request.rating.RatingRequest;
import com.example.homeservice.service.rating.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/rating")
public class RatingController {
    private final RatingService ratingService;

    @PostMapping("")
    @PreAuthorize("hasRole('CLIENT')")
    public ResponseEntity ratingRoom(@AuthenticationPrincipal UserPrincipal userPrincipal, @RequestBody RatingRequest ratingRequest, HttpServletRequest request) throws ParseException {
        return ratingService.ratingHome(userPrincipal.getId(), ratingRequest);
    }

    @GetMapping("/{idHome}")
    public ResponseEntity getRatingByHome(@PathVariable UUID idHome) {
        return ratingService.getRatingHome(idHome);
    }

    @GetMapping("/owner/{idUser}")
    public ResponseEntity getRatingByOwner(@PathVariable UUID idUser) {
        return ratingService.getRatingByOwner(idUser);
    }
}
