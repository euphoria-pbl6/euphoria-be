package com.example.homeservice.controller;

import com.euphoria.common.domain.model.UserPrincipal;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.exception.BadRequestException;
import com.example.homeservice.domain.enums.BookingStatus;
import com.example.homeservice.payload.request.booking.CancelBookingRequest;
import com.example.homeservice.service.booking.BookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/owner/booking")
public class OwnerBookingController {
  private final BookingService bookingService;

  @PatchMapping("{id}/accept")
  @PreAuthorize("hasRole('OWNER')")
  public ResponseEntity acceptBookingRequest(
      @AuthenticationPrincipal UserPrincipal userPrincipal, @PathVariable UUID id) {
    return ResponseEntity.ok(bookingService.acceptBooking(userPrincipal.getId(), id));
  }

  @PatchMapping("{id}/reject")
  @PreAuthorize("hasRole('OWNER')")
  public ResponseEntity rejectBookingRequest(
      @AuthenticationPrincipal UserPrincipal userPrincipal,
      @PathVariable UUID id,
      @RequestBody CancelBookingRequest cancelBookingRequest) {
    return ResponseEntity.ok(
        bookingService.rejectBooking(userPrincipal.getId(), id, cancelBookingRequest));
  }

  @GetMapping
  @PreAuthorize("hasRole('OWNER')")
  public ResponseEntity getBookingList(
      @AuthenticationPrincipal UserPrincipal userPrincipal,
      HttpServletRequest request,
      @RequestParam(value = "status", defaultValue = "ALL") String status,
      @RequestParam(value = "page", defaultValue = "1") int page,
      @RequestParam(value = "paging", defaultValue = "10") int paging) {
    if (status.equalsIgnoreCase("ALL")
        || status.equalsIgnoreCase(BookingStatus.ACCEPTED.toString())
        || status.equalsIgnoreCase(BookingStatus.WAITING.toString())
        || status.equalsIgnoreCase(BookingStatus.CANCEL.toString())
        || status.equalsIgnoreCase(BookingStatus.REJECT.toString())) {
      if (status.equalsIgnoreCase("ALL")) status = "%";
        return ResponseEntity.ok(bookingService.getListBookingByOwner(userPrincipal.getId(), status,page,paging));


    } else {
      throw new BadRequestException(ErrorMessage.BOOKING_STATUS_NOT_MATCH);
    }
  }

  @GetMapping("{id}")
  @PreAuthorize("hasRole('OWNER')")
  public ResponseEntity getBookingDetailByOwner(
      @AuthenticationPrincipal UserPrincipal userPrincipal, @PathVariable UUID id) {
    return ResponseEntity.ok(bookingService.getBookingDetailByOwner(userPrincipal.getId(), id));
  }
}
