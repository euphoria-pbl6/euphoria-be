package com.example.homeservice.controller;

import com.example.homeservice.service.HomeService;
import com.example.homeservice.service.booking.BookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/files")
public class FileController {
    private final HomeService homeService;
    @PostMapping
    public ResponseEntity uploadImage(@RequestPart("files") List<MultipartFile> multipartFiles){
        return homeService.uploadImage(multipartFiles);
    }
    }
