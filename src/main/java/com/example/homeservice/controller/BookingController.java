package com.example.homeservice.controller;


import com.euphoria.common.domain.model.UserPrincipal;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.exception.BadRequestException;
import com.example.homeservice.domain.enums.BookingStatus;
import com.example.homeservice.payload.request.booking.BookingRequest;
import com.example.homeservice.payload.request.booking.CancelBookingRequest;
import com.example.homeservice.service.HomeService;
import com.example.homeservice.service.booking.BookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/booking")
public class BookingController {
    private final BookingService bookingService;

    @PostMapping
    @PreAuthorize("hasRole('CLIENT')")
    public ResponseEntity bookingRoom(@AuthenticationPrincipal UserPrincipal userPrincipal, @RequestBody BookingRequest bookingRequest, HttpServletRequest request) throws ParseException {
        return ResponseEntity.ok(bookingService.bookingRoom(bookingRequest, userPrincipal.getId(),request.getLocalAddr()));
    }
    @GetMapping("/home/{id}/booked-date")
    public ResponseEntity getHomeBookedDate(@PathVariable UUID id)  {
        return ResponseEntity.ok(bookingService.getBookedDate(id));
    }

    @GetMapping("/")
    @PreAuthorize("hasRole('CLIENT')")
    public ResponseEntity getBookingList(@AuthenticationPrincipal UserPrincipal userPrincipal, HttpServletRequest request,
                                         @RequestParam(value = "status", defaultValue = "ALL") String status,
                                         @RequestParam(value = "page", defaultValue = "1") int page,
                                         @RequestParam(value = "paging", defaultValue = "10") int paging) {
        if (status.equalsIgnoreCase("ALL") || status.equalsIgnoreCase(BookingStatus.ACCEPTED.toString()) || status.equalsIgnoreCase(BookingStatus.WAITING.toString())
                || status.equalsIgnoreCase(BookingStatus.CANCEL.toString()) || status.equalsIgnoreCase(BookingStatus.REJECT.toString())) {
            if (status.equalsIgnoreCase("ALL")) status = "%";
      return ResponseEntity.ok(
          bookingService.getListBookingByClient(userPrincipal.getId(), status,page,paging));

        } else {
            throw new BadRequestException(ErrorMessage.BOOKING_STATUS_NOT_MATCH);
        }
    }

  @GetMapping("/{id}")
  @PreAuthorize("hasRole('CLIENT')")
  public ResponseEntity getBookingDetail(
      @AuthenticationPrincipal UserPrincipal userPrincipal, @PathVariable UUID id) {
            return ResponseEntity.ok(bookingService.getBookingDetailByClient(userPrincipal.getId(),id));
    }
    @PatchMapping("{id}/cancel")
    @PreAuthorize("hasRole('CLIENT')")
    public ResponseEntity cancelBookingRequest(@AuthenticationPrincipal UserPrincipal userPrincipal, @PathVariable UUID id, @RequestBody CancelBookingRequest cancelBookingRequest){
        return ResponseEntity.ok(bookingService.cancelBooking(userPrincipal.getId(),id,cancelBookingRequest));
    }

}
