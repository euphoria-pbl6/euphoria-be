package com.example.homeservice.service.payment.impl;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.common.CommonFunction;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.constant.ResponseCodeVNPay;
import com.example.homeservice.common.exception.ResourceNotFoundException;
import com.example.homeservice.config.properties.VNPayProperties;
import com.example.homeservice.domain.enums.BookingStatus;
import com.example.homeservice.domain.enums.TransactionStatus;
import com.example.homeservice.domain.model.booking.Booking;
import com.example.homeservice.domain.model.booking.Transactions;
import com.example.homeservice.payload.request.payment.PaymentRequest;
import com.example.homeservice.payload.request.payment.PaymentResultRequest;
import com.example.homeservice.repository.booking.BookingRepository;
import com.example.homeservice.repository.booking.TransactionRepository;
import com.example.homeservice.service.payment.PaymentService;
import lombok.RequiredArgsConstructor;
import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.scheduling.JobScheduler;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {
  private final VNPayProperties vnPayProperties;
  private final TransactionRepository transactionRepository;
  private final BookingRepository bookingRepository;
  private final JobScheduler jobScheduler;

  @Override
  public Transactions makePayment(PaymentRequest paymentReq) throws UnsupportedEncodingException {

    String vnp_Version = "2.1.0";
    String vnp_Command = "pay";

    String vnp_TxnRef = String.format("%08d", new SecureRandom().nextInt(10_000_000));
    String vnp_IpAddr = paymentReq.getIpAddr();
    String vnp_TmnCode = vnPayProperties.getTmnCode();
    String vnp_OrderInfo = paymentReq.getVnpOrderInfo();
    String orderType = "other";
    String locate = paymentReq.getLanguage();
    BigDecimal amount = paymentReq.getAmount().multiply(new BigDecimal("100"));

    Map vnp_Params = new HashMap<>();
    vnp_Params.put("vnp_Version", vnp_Version);
    vnp_Params.put("vnp_Command", vnp_Command);
    vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
    vnp_Params.put("vnp_Amount", String.valueOf(amount.longValue()));
    vnp_Params.put("vnp_CurrCode", "VND");
    vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
    vnp_Params.put("vnp_OrderInfo", vnp_OrderInfo);
    vnp_Params.put("vnp_OrderType", orderType);

    if (locate != null && !locate.isEmpty()) {
      vnp_Params.put("vnp_Locale", locate);
    } else {
      vnp_Params.put("vnp_Locale", "vn");
    }
    vnp_Params.put("vnp_ReturnUrl", paymentReq.getReturnURL());
    vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

    Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    String vnp_CreateDate = formatter.format(cld.getTime());
    vnp_Params.put("vnp_CreateDate", vnp_CreateDate);

    cld.add(Calendar.MINUTE, 1);
    String vnp_ExpireDate = formatter.format(cld.getTime());
    vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);
    List fieldNames = new ArrayList(vnp_Params.keySet());
    Collections.sort(fieldNames);
    StringBuilder hashData = new StringBuilder();
    StringBuilder query = new StringBuilder();
    Iterator itr = fieldNames.iterator();
    while (itr.hasNext()) {
      String fieldName = (String) itr.next();
      String fieldValue = (String) vnp_Params.get(fieldName);
      if ((fieldValue != null) && (fieldValue.length() > 0)) {
        // Build hash data
        hashData.append(fieldName);
        hashData.append('=');
        hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
        // Build query
        query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
        query.append('=');
        query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
        if (itr.hasNext()) {
          query.append('&');
          hashData.append('&');
        }
      }
    }
    String queryUrl = query.toString();
    String vnp_SecureHash =
        CommonFunction.hmacSha512(vnPayProperties.getHashSecret(), hashData.toString());
    queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
    String paymentUrl = vnPayProperties.getPaymentUrl() + "?" + queryUrl;

    Transactions transactions = new Transactions();
    transactions.setAmount(paymentReq.getAmount());
    transactions.setStatus(TransactionStatus.PENDING);
    transactions.setTxtRef(vnp_TxnRef);
    transactions.setTimeOver(cld.getTime());
    transactions.setPaymentURL(paymentUrl);
    transactions.setCreatedAt(new Timestamp(new Date().getTime()));
    transactions.setUpdatedAt(new Timestamp(new Date().getTime()));
    jobScheduler.schedule(LocalDateTime.now().plusMinutes(2),()-> checkPayment(vnp_TxnRef,transactions.getAmount()));
    return transactionRepository.save(transactions);
  }

  @Override
  public ResponseCustom getPayment(PaymentResultRequest request) throws ParseException {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    Timestamp transOn = new Timestamp(formatter.parse(request.getTransferOn()).getTime());
    Transactions transactions =
        transactionRepository.getTransactionsByTxtRefAndCreatedAt(
            request.getTxtRef(), request.getAmount().divide(new BigDecimal("100")));

    if (transactions == null) {
      throw new ResourceNotFoundException(ErrorMessage.TRANSACTION_NOT_FOUND);
    }
    Booking booking = bookingRepository.getBookingByTransactionsId(transactions.getId());

    if (request.getResponseCode().equals(ResponseCodeVNPay.SUCCESS_CODE)) {
      transactions.setStatus(TransactionStatus.SUCCESS);
      booking.setStatus(BookingStatus.PAID);
    } else {
      transactions.setStatus(TransactionStatus.FAIL);
      booking.setStatus(BookingStatus.CANCEL);
    }
    transactions.setUpdatedAt(new Timestamp(new Date().getTime()));
    transactions.setTransferOn(transOn);
    transactions.setBankTranNo(request.getTransactionNo());
    transactionRepository.save(transactions);
    booking.setUpdatedAt(new Timestamp(new Date().getTime()));
    booking.setTransactions(transactions);

    return ResponseCustom.successResponse(bookingRepository.save(booking));
  }

  @Job(name = "Check payment")
  public void checkPayment(String txtRef,BigDecimal amount){
    Transactions transactions =
            transactionRepository.getTransactionsByTxtRefAndCreatedAt(
                    txtRef,amount);
    System.out.println("Job trigger");
    if(transactions.getStatus() == TransactionStatus.PENDING){
      Booking booking = bookingRepository.getBookingByTransactionsId(transactions.getId());
      booking.setStatus(BookingStatus.CANCEL);
      transactions.setStatus(TransactionStatus.FAIL);
      bookingRepository.save(booking);
      transactionRepository.save(transactions);
    }
  }
}
