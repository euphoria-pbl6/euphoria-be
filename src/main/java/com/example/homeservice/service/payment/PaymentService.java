package com.example.homeservice.service.payment;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.domain.model.booking.Transactions;
import com.example.homeservice.payload.request.payment.PaymentRequest;
import com.example.homeservice.payload.request.payment.PaymentResultRequest;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;

public interface PaymentService {
    Transactions makePayment(PaymentRequest request) throws UnsupportedEncodingException;
    ResponseCustom getPayment(PaymentResultRequest request) throws ParseException;
}
