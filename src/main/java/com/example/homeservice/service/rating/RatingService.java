package com.example.homeservice.service.rating;

import com.example.homeservice.payload.request.rating.RatingRequest;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface RatingService {
    public ResponseEntity ratingHome(UUID idUser, RatingRequest ratingRequest);

    public ResponseEntity getRatingHome(UUID idHome);

    public ResponseEntity getRatingByOwner(UUID idUser);
}
