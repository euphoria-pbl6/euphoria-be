package com.example.homeservice.service.rating.impl;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.exception.NotFoundException;
import com.example.homeservice.domain.enums.RatingStatus;
import com.example.homeservice.domain.model.Rating;
import com.example.homeservice.domain.model.booking.Booking;
import com.example.homeservice.payload.dto.BookingDTO;
import com.example.homeservice.payload.request.rating.RatingRequest;
import com.example.homeservice.payload.response.Rating.RatingResponse;
import com.example.homeservice.payload.response.Rating.ReviewResponse;
import com.example.homeservice.repository.booking.BookingRepository;
import com.example.homeservice.repository.rating.RatingRepository;
import com.example.homeservice.service.rating.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
@Service
public class RatingServiceImpl implements RatingService {
    private final BookingRepository bookingRepository;
    private final RatingRepository ratingRepository;

    @Override
    public ResponseEntity ratingHome(UUID idUser, RatingRequest ratingRequest) {
        bookingRepository
                        .getBookingDetailByClient(idUser, ratingRequest.getIdBooking(), RatingStatus.NOT_YET_RATED.name())
                        .orElseThrow(
                                () -> new NotFoundException(ErrorMessage.NOT_RATED));

        Booking booking = bookingRepository.findById(ratingRequest.getIdBooking()).get();

        Rating rating = new Rating(
                ratingRequest.getCleanliness(),
                ratingRequest.getAccuracy(),
                ratingRequest.getCommunication(),
                ratingRequest.getCheckIn(),
                ratingRequest.getLocation(),
                ratingRequest.getValue(),
                ratingRequest.getReview(),
                LocalDate.now(),
                idUser,
                booking);

        ratingRepository.save(rating);

        booking.setRating(RatingStatus.RATED);
        bookingRepository.save(booking);

        return ResponseEntity.ok(ResponseCustom.successResponse(rating));
    }

    @Override
    public ResponseEntity getRatingHome(UUID idHome) {
        List<Rating> ratings = ratingRepository.findRatingByHome(idHome);

        RatingResponse ratingResponse = new RatingResponse();

        Double cleanliness = 0.0;
        Double value = 0.0;
        Double location = 0.0;
        Double accuracy = 0.0;
        Double checkIn = 0.0;
        Double communication = 0.0;

        List<ReviewResponse> reviewResponses = new ArrayList<>();

        for(Rating rating : ratings){
            cleanliness += rating.getCleanliness();
            value += rating.getValue();
            location += rating.getLocation();
            accuracy += rating.getAccuracy();
            checkIn += rating.getCheckIn();
            communication += rating.getCommunication();

            reviewResponses.add(new ReviewResponse(rating.getIdUser(), rating.getReview(), rating.getDateCreate(), idHome));
        }

        ratingResponse.setCleanliness(cleanliness);
        ratingResponse.setAccuracy(accuracy);
        ratingResponse.setLocation(location);
        ratingResponse.setCommunication(communication);
        ratingResponse.setValue(value);
        ratingResponse.setCheckIn(checkIn);
        ratingResponse.setReviewResponses(reviewResponses);

        return ResponseEntity.ok(ResponseCustom.successResponse(ratingResponse));
    }

    @Override
    public ResponseEntity getRatingByOwner(UUID idUser) {
        List<ReviewResponse> reviewResponses = new ArrayList<>();

        List<BookingDTO> bookingDTOS = bookingRepository.getBookingByRating(idUser.toString(), RatingStatus.RATED.name());

        for(BookingDTO bookingDTO : bookingDTOS){
            Rating rating = ratingRepository.findByBooking(UUID.fromString(bookingDTO.getId()))
                    .orElseThrow(
                            () -> new NotFoundException(ErrorMessage.RATING_NOT_EXISTS));

            reviewResponses.add(new ReviewResponse(rating.getIdUser(), rating.getReview(), rating.getDateCreate(), UUID.fromString(bookingDTO.getHomeId())));
        }

        return ResponseEntity.ok(ResponseCustom.successResponse(reviewResponses));
    }
}
