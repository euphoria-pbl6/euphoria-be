package com.example.homeservice.service.util;


import com.example.homeservice.common.exception.ResourceNotFoundException;

import java.io.File;

public final class FileUtils {
    private FileUtils() {}

    public static void deleteFile(File file) {
        if (file.delete()) {
            return;
        }
        throw new ResourceNotFoundException("fail");
    }
}
