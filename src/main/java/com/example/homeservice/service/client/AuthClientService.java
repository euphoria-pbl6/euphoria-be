package com.example.homeservice.service.client;

import com.euphoria.common.domain.model.UserInfo;
import com.euphoria.common.domain.model.UserPrincipal;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name="AUTH-SERVICE",url="http://localhost:8512")
public interface AuthClientService {
    @GetMapping("/auth/validate")
    UserPrincipal validate(@RequestHeader("Authorization") String bearerToken);
    @GetMapping("/user/{id}")
    UserInfo getUserByIdUserParam(@PathVariable("id") String id);
}
