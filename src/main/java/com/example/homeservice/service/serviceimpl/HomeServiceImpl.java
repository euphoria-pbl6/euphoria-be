package com.example.homeservice.service.serviceimpl;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.exception.ResourceNotFoundException;
import com.example.homeservice.common.general.PageInfo;
import com.example.homeservice.config.properties.FileServiceConfigProperties;
import com.example.homeservice.domain.model.*;
import com.example.homeservice.domain.model.booking.Booking;
import com.example.homeservice.payload.request.HomeRequest;
import com.example.homeservice.payload.response.HomeDetailResponse;
import com.example.homeservice.payload.response.HomeResponse;
import com.example.homeservice.repository.*;
import com.example.homeservice.repository.booking.BookingRepository;
import com.example.homeservice.service.HomeService;
import com.example.homeservice.service.client.AuthClientService;
import com.example.homeservice.service.util.FileUtils;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@RequiredArgsConstructor
@Slf4j
@Service
public class HomeServiceImpl implements HomeService {
    private final HomeRepository homeRepository;

    private final AuthClientService authClientService;

    private final CategoryRepository categoryRepository;

    private final BathroomRepository bathroomRepository;

    private final BedroomAndLaundryRepository bedroomAndLaundryRepository;

    private final EntertainmentRepository entertainmentRepository;

    private final FamilyRepository familyRepository;

    private final HeatingAndCoolingRepository heatingAndCoolingRepository;

    private final HomeSafetyRepository homeSafetyRepository;

    private final InternetAndOfficeRepository internetAndOfficeRepository;

    private final KitchenAndDiningRepository kitchenAndDiningRepository;

    private final LocationFeatureRepository locationFeatureRepository;

    private final OutdoorRepository outdoorRepository;

    private final ParkingAndFacilityRepository parkingAndFacilityRepository;

    private final ServiceRepository serviceRepository;

    private final StatusRepository statusRepository;

    private final BookingRepository bookingRepository;

  @Override
  public ResponseEntity getAll(int page, int paging, String orderBy) {
    Sort sort =
        "ASC".equalsIgnoreCase(orderBy)
            ? Sort.by("dateCreate").ascending()
            : Sort.by("dateCreate").descending();
    Pageable pageable = PageRequest.of(page - 1, paging, sort);
        Status status = statusRepository.findByName(EStatus.APPROVED);
        Page<Home> homes = homeRepository.findHomeByStatusId(status.getId(),pageable);

        PageInfo pageInfo =
                new PageInfo(pageable.getPageNumber() + 1, homes.getTotalPages(), homes.getTotalElements());

        List<HomeResponse> rs = new ArrayList<>();

        for(Home home : homes){
            HomeResponse homeResponse = new HomeResponse(home);

            rs.add(homeResponse);
        }

        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(rs, pageInfo));
    }

    @Override
    public ResponseEntity getHome(UUID id) {
        Home home = homeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.HOME_NOT_EXISTS));

        HomeDetailResponse homeDetailResponse = home(home);

        @SuppressWarnings("unchecked")
        Object user = authClientService.getUserByIdUserParam(home.getIdUser());

        homeDetailResponse.setUser(user);

        return ResponseEntity.ok(ResponseCustom.successResponse(homeDetailResponse));
    }

    private final FileServiceConfigProperties properties;

    private void checkSizeFile(File file) {
        long size;
        try {
            size = Files.size(file.toPath());
            if (size > 10485760) {
                FileUtils.deleteFile(file);
                throw new ResourceNotFoundException("MAXIMUM_UPLOAD_SIZE_EXCEEDED");
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    private File convertMultiPartToFile(MultipartFile file) {
        File convFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        try (FileOutputStream fos = new FileOutputStream(convFile)) {
            fos.write(file.getBytes());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return convFile;
    }

    private String generateFileName(String fileName) {
        return new Date().getTime() + "-" + fileName.replace(" ", "_");
    }

    public String uploadGCS(String fileName, File file) throws IOException {
        Storage storage =
                StorageOptions.newBuilder()
                        .setProjectId(properties.getProjectId())
                        .setCredentials(
                                GoogleCredentials.fromStream(Files.newInputStream(Paths.get(properties.getCredentialPath()))))
                        .build()
                        .getService();
        BlobId blobId = BlobId.of(properties.getBucketName(), fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("image/png").build();
        storage.create(blobInfo, Files.readAllBytes(Paths.get(file.getPath())));
        return getUrl(fileName);
    }
    private String getUrl(String filename){
        return properties.getUrlFirebase().replace("%s",filename);
    }


    private String getMineType(File file) throws IOException {
        Tika tika = new Tika();
        return tika.detect(file);
    }


    @Override
    public HomeDetailResponse createHome(String idUser, HomeRequest homeRequest) {
        Home home = new Home(homeRequest);
        home.setDateCreate(new Date());
        home.setIdUser(idUser);
        Category category = categoryRepository.findById(homeRequest.getCategory())
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.CATEGORY_NOT_EXISTS));
        home.setCategory(category);
        Status status = statusRepository.findByName(EStatus.WAITING_APPROVE);
        home.setStatus(status);

        homeRepository.save(home);

        return home(home);
    }

  @Override
  public ResponseEntity getHomeByIdUser(String idUser, int page, int paging, String orderBy) {
    Sort sort =
        "ASC".equalsIgnoreCase(orderBy)
            ? Sort.by("dateCreate").ascending()
            : Sort.by("dateCreate").descending();
    Pageable pageable = PageRequest.of(page - 1, paging, sort);

        Page<Home> homes = homeRepository.findHomeByIdUser(idUser, pageable)
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.HOME_NOT_EXISTS));

        PageInfo pageInfo =
                new PageInfo(pageable.getPageNumber() + 1, homes.getTotalPages(), homes.getTotalElements());

        List<HomeDetailResponse> rs = new ArrayList<>();

        Object user = authClientService.getUserByIdUserParam(idUser);

        for(Home home : homes){
            HomeDetailResponse homeDetailResponse = home(home);

            homeDetailResponse.setUser(user);

            rs.add(homeDetailResponse);
        }

    return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(rs,pageInfo));
    }

    @Override
    public ResponseEntity getCategories() {

        return ResponseEntity.ok(ResponseCustom.successResponse(categoryRepository.findAll()));
    }

    @Override
    public ResponseEntity getBathroom() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","bath_room");
        res.put("data",bathroomRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponse(res));
    }

    @Override
    public ResponseEntity getBedroomAndLaundry() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","bedroom_laundry");
        res.put("data",bedroomAndLaundryRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponse(res));
    }

    @Override
    public ResponseEntity getEntertainment() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","entertainment");
        res.put("data",entertainmentRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponse(res));
    }

    @Override
    public ResponseEntity getFamily() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","family");
        res.put("data",familyRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponse(res));
    }

    @Override
    public ResponseEntity getHeatingAndCooling() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","heating_cooling");
        res.put("data",heatingAndCoolingRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponse(res));
    }

    @Override
    public ResponseEntity getHomeSafety() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","home_safety");
        res.put("data",homeSafetyRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(res, null));
    }

    @Override
    public ResponseEntity getInternetAndOffice() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","internet_office");
        res.put("data",internetAndOfficeRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(res, null));
    }

    @Override
    public ResponseEntity getKitchenAndDining() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","kitchen_dining");
        res.put("data",kitchenAndDiningRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(res, null));
    }

    @Override
    public ResponseEntity getLocationFeature() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","location_feature");
        res.put("data",locationFeatureRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(res, null));
    }

    @Override
    public ResponseEntity getOutdoor() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","outdoor");
        res.put("data",outdoorRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(res, null));
    }

    @Override
    public ResponseEntity getParkingAndFacilities() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","parking_facilities");
        res.put("data",parkingAndFacilityRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(res, null));
    }

    @Override
    public ResponseEntity getServices() {
        Map<String,Object> res = new HashMap<>();
        res.put("title","service");
        res.put("data",serviceRepository.findAll());
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(res, null));
    }

      @Override
      public ResponseEntity getHome(int idCategory, int page, int paging, String orderBy) {
        Sort sort =
            "ASC".equalsIgnoreCase(orderBy)
                ? Sort.by("dateCreate").ascending()
                : Sort.by("dateCreate").descending();
        Pageable pageable = PageRequest.of(page - 1, paging, sort);

          Page<Home> homes;

        if(idCategory == 0){
            homes = homeRepository.findAllHome(EStatus.APPROVED, pageable);
        }
        else{
            homes = homeRepository.findHomeByCategory(idCategory, EStatus.APPROVED, pageable);
        }

        PageInfo pageInfo =
                new PageInfo(pageable.getPageNumber() + 1, homes.getTotalPages(), homes.getTotalElements());

        List<HomeResponse> rs = new ArrayList<>();

        for(Home home : homes){
            HomeResponse homeResponse = new HomeResponse(home);

            rs.add(homeResponse);
        }

        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(rs, pageInfo));
    }

    @Override
    public ResponseEntity uploadImage(List<MultipartFile> files) {
        String fileUrl;
        List<String> rs = new ArrayList<>();

        try {
            for(MultipartFile multipartFile : files){
                File file = convertMultiPartToFile(multipartFile);
                this.checkSizeFile(file);

                String mimeType = this.getMineType(file);
                if (mimeType.startsWith("image")) {
                    String fileName =
                            generateFileName(Objects.requireNonNull(multipartFile.getOriginalFilename()));
                    fileUrl = this.uploadGCS(fileName, file);
                    FileUtils.deleteFile(file);

                    rs.add(fileUrl);
                } else {
                    FileUtils.deleteFile(file);
                    throw new ResourceNotFoundException(ErrorMessage.FILE_NOT_FORMAT);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(rs, null));
    }

    @Override
    public ResponseEntity getHomeWaitingApprove(int page, int paging) {
        Pageable pageable = PageRequest.of(page - 1, paging);

        Status status = statusRepository.findByName(EStatus.WAITING_APPROVE);

        Page<Home> homes = homeRepository.findHomeByStatusId(status.getId(), pageable);

        PageInfo pageInfo =
                new PageInfo(pageable.getPageNumber() + 1, homes.getTotalPages(), homes.getTotalElements());

        List<HomeResponse> rs = new ArrayList<>();

        for(Home home : homes){
            HomeResponse homeResponse = new HomeResponse(home);

            rs.add(homeResponse);
        }

        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(rs, pageInfo));
    }

    @Override
    public ResponseEntity confirmHome(UUID idHome, String status) {
        Home home = homeRepository.findById(idHome)
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.HOME_NOT_EXISTS));

        if(home.getStatus().getName() != EStatus.WAITING_APPROVE){
            throw new ResourceNotFoundException(ErrorMessage.HOME_CONFIRMED);
        }

        if(status.equals("APPROVED")){
            home.setStatus(statusRepository.findByName(EStatus.APPROVED));
        }
        else{
            home.setStatus(statusRepository.findByName(EStatus.DISAPPROVED));
        }
        homeRepository.save(home);
        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(new HomeResponse(home), null));
    }

    public HomeDetailResponse home(Home home){
        HomeDetailResponse homeDetailResponse = new HomeDetailResponse();
        homeDetailResponse.setId(home.getId());
        homeDetailResponse.setName(home.getName());
        homeDetailResponse.setDescription(home.getDescription());
        homeDetailResponse.setAddress(home.getAddress());
        homeDetailResponse.setNumOfGuests(home.getNumOfGuests());
        homeDetailResponse.setNumOfPets(home.getNumOfPets());
        homeDetailResponse.setNumOfBabies(home.getNumOfBabies());
        homeDetailResponse.setNumOfBeds(home.getNumOfBeds());
        homeDetailResponse.setNumOfBedrooms(home.getNumOfBedrooms());
        homeDetailResponse.setNumOfBathrooms(home.getNumOfBathrooms());
        homeDetailResponse.setPrice(home.getPrice());
        homeDetailResponse.setPriceService(home.getPriceService());
        homeDetailResponse.setPriceForPet(home.getPriceForPet());
        homeDetailResponse.setCategory(home.getCategory());
        homeDetailResponse.setFiles(home.getFiles());
        homeDetailResponse.setStartAt(home.getStartAt());
        homeDetailResponse.setEndAt(home.getEndAt());
        homeDetailResponse.setDateCreate(home.getDateCreate());

        List<Bathroom> bathrooms = new ArrayList<>();
        for(String bathroom : home.getBathroom()){
            if(bathroomRepository.existsByTitle(bathroom)){
                bathrooms.add(bathroomRepository.findByTitle(bathroom));
            }
            else{
                bathrooms.add(new Bathroom(0, bathroom, null));
            }
        }

        List<BedroomAndLaundry> bedroomAndLaundries = new ArrayList<>();
        for(String bedroomAndLaundry : home.getBedroomAndLaundry()){
            if(bedroomAndLaundryRepository.existsByTitle(bedroomAndLaundry)){
                bedroomAndLaundries.add(bedroomAndLaundryRepository.findByTitle(bedroomAndLaundry));
            }
            else{
                bedroomAndLaundries.add(new BedroomAndLaundry(0, bedroomAndLaundry, null));
            }
        }

        List<Entertainment> entertainments = new ArrayList<>();
        for(String entertainment : home.getEntertainment()){
            if(entertainmentRepository.existsByTitle(entertainment)){
                entertainments.add(entertainmentRepository.findByTitle(entertainment));
            }
            else{
                entertainments.add(new Entertainment(0, entertainment, null));
            }
        }

        List<Family> families = new ArrayList<>();
        for(String family : home.getFamily()){
            if(familyRepository.existsByTitle(family)){
                families.add(familyRepository.findByTitle(family));
            }
            else{
                families.add(new Family(0, family, null));
            }

        }

        List<HeatingAndCooling> heatingAndCoolings = new ArrayList<>();
        for(String heatingAndCooling : home.getHeatingAndCooling()){
            if(heatingAndCoolingRepository.existsByTitle(heatingAndCooling)){
                heatingAndCoolings.add(heatingAndCoolingRepository.findByTitle(heatingAndCooling));
            }
            else{
                heatingAndCoolings.add(new HeatingAndCooling(0, heatingAndCooling, null));
            }
        }

        List<HomeSafety> homeSafeties = new ArrayList<>();
        for(String homeSafety : home.getHomeSafety()){
            if(homeSafetyRepository.existsByTitle(homeSafety)){
                homeSafeties.add(homeSafetyRepository.findByTitle(homeSafety));
            }
            else{
                homeSafeties.add(new HomeSafety(0, homeSafety, null));
            }
        }

        List<InternetAndOffice> internetAndOffices = new ArrayList<>();
        for(String internetAndOffice : home.getInternetAndOffice()){
            if(internetAndOfficeRepository.existsByTitle(internetAndOffice)){
                internetAndOffices.add(internetAndOfficeRepository.findByTitle(internetAndOffice));
            }
            else{
                internetAndOffices.add(new InternetAndOffice(0, internetAndOffice, null));
            }
        }

        List<KitchenAndDining> kitchenAndDinings = new ArrayList<>();
        for(String kitchenAndDining : home.getKitchenAndDining()){
            if(kitchenAndDiningRepository.existsByTitle(kitchenAndDining)){
                kitchenAndDinings.add(kitchenAndDiningRepository.findByTitle(kitchenAndDining));
            }
            else{
                kitchenAndDinings.add(new KitchenAndDining(0, kitchenAndDining, null));
            }
        }

        List<LocationFeatures> locationFeatures = new ArrayList<>();
        for(String locationFeature : home.getLocationFeature()){
            if(locationFeatureRepository.existsByTitle(locationFeature)){
                locationFeatures.add(locationFeatureRepository.findByTitle(locationFeature));
            }
            else{
                locationFeatures.add(new LocationFeatures(0, locationFeature, null));
            }
        }

        List<Outdoor> outdoors = new ArrayList<>();
        for(String outdoor : home.getOutdoor()){
            if(outdoorRepository.existsByTitle(outdoor)){
                outdoors.add(outdoorRepository.findByTitle(outdoor));
            }
            else{
                outdoors.add(new Outdoor(0, outdoor, null));
            }
        }

        List<ParkingAndFacilities> parkingAndFacilities = new ArrayList<>();
        for(String parkingAndFacility : home.getParkingAndFacilities()){
            if(parkingAndFacilityRepository.existsByTitle(parkingAndFacility)){
                parkingAndFacilities.add(parkingAndFacilityRepository.findByTitle(parkingAndFacility));
            }
            else{
                parkingAndFacilities.add(new ParkingAndFacilities(0, parkingAndFacility, null));
            }
        }

        List<Services> services = new ArrayList<>();
        for(String service : home.getServices()){
            if(serviceRepository.existsByTitle(service)){
                services.add(serviceRepository.findByTitle(service));
            }
            else{
                services.add(new Services(0, service, null));
            }

        }

        homeDetailResponse.setBathroom(bathrooms);
        homeDetailResponse.setBedroomAndLaundry(bedroomAndLaundries);
        homeDetailResponse.setEntertainment(entertainments);
        homeDetailResponse.setFamily(families);
        homeDetailResponse.setHeatingAndCooling(heatingAndCoolings);
        homeDetailResponse.setHomeSafety(homeSafeties);
        homeDetailResponse.setInternetAndOffice(internetAndOffices);
        homeDetailResponse.setKitchenAndDining(kitchenAndDinings);
        homeDetailResponse.setLocationFeature(locationFeatures);
        homeDetailResponse.setOutdoor(outdoors);
        homeDetailResponse.setParkingAndFacilities(parkingAndFacilities);
        homeDetailResponse.setServices(services);

        homeDetailResponse.setUser(authClientService.getUserByIdUserParam(home.getIdUser()));
        homeDetailResponse.setStatus(home.getStatus().getName().name());

        return homeDetailResponse;
    }

    @Override
    public ResponseEntity editHome(UUID idHome, HomeRequest homeRequest){
        Home home = homeRepository.findById(idHome)
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.HOME_NOT_EXISTS));

        List<Booking> bookings = bookingRepository.getBookingByHome(idHome, "ACCEPT");
        if(bookings.size() > 0){
            throw new ResourceNotFoundException(ErrorMessage.HOME_NOT_EDIT);
        }

        Home home1 = new Home(homeRequest);
        home1.setId(home.getId());

        homeRepository.save(home1);

        return ResponseEntity.ok(ResponseCustom.successResponseWithMeta(home(home), null));
    }
}