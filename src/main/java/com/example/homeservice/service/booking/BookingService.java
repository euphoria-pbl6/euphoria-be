package com.example.homeservice.service.booking;

import com.euphoria.common.ResponseCustom;
import com.example.homeservice.payload.request.booking.BookingRequest;
import com.example.homeservice.payload.request.booking.CancelBookingRequest;

import java.text.ParseException;
import java.util.UUID;

public interface BookingService {
    ResponseCustom bookingRoom(BookingRequest bookingRequest, UUID userId,String ipAddr) throws ParseException;
    ResponseCustom getListBookingByClient(UUID userId, String status,int page, int paging);

    ResponseCustom acceptBooking(UUID userId, UUID bookingId);
    ResponseCustom rejectBooking(UUID userId, UUID bookingId, CancelBookingRequest cancelBookingRequest);
    ResponseCustom cancelBooking(UUID userId,UUID bookingId,CancelBookingRequest cancelBookingRequest);
    ResponseCustom getBookedDate(UUID homeId);
    ResponseCustom getListBookingByOwner(UUID userId,String status, int page, int paging);
    ResponseCustom getBookingDetailByClient(UUID userId,UUID bookingId);
    ResponseCustom getBookingDetailByOwner(UUID userId,UUID bookingId);


}
