package com.example.homeservice.service.booking.impl;

import com.euphoria.common.ResponseCustom;
import com.euphoria.common.domain.model.UserInfo;
import com.example.homeservice.common.CommonFunction;
import com.example.homeservice.common.constant.ErrorMessage;
import com.example.homeservice.common.exception.BadRequestException;
import com.example.homeservice.common.exception.NotFoundException;
import com.example.homeservice.common.exception.ResourceNotFoundException;
import com.example.homeservice.common.general.PageInfo;
import com.example.homeservice.domain.enums.BookingStatus;
import com.example.homeservice.domain.enums.RatingStatus;
import com.example.homeservice.domain.model.Home;
import com.example.homeservice.domain.model.booking.Booking;
import com.example.homeservice.domain.model.booking.Transactions;
import com.example.homeservice.payload.dto.BookingDTO;
import com.example.homeservice.payload.request.booking.BookingRequest;
import com.example.homeservice.payload.request.booking.CancelBookingRequest;
import com.example.homeservice.payload.request.payment.PaymentRequest;
import com.example.homeservice.payload.response.HomeResponse;
import com.example.homeservice.payload.response.booking.BookingResponse;
import com.example.homeservice.payload.response.booking.TransactionsResponse;
import com.example.homeservice.repository.HomeRepository;
import com.example.homeservice.repository.booking.BookingRepository;
import com.example.homeservice.repository.booking.TransactionRepository;
import com.example.homeservice.service.HomeService;
import com.example.homeservice.service.booking.BookingService;
import com.example.homeservice.service.client.AuthClientService;
import com.example.homeservice.service.payment.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.DAYS;

@RequiredArgsConstructor
@Service
public class BookingServiceImpl implements BookingService {
  private final BookingRepository bookingRepository;
  private final HomeService homeService;
  private final PaymentService paymentService;
  private final AuthClientService authClientService;
  private final HomeRepository homeRepository;
  private final TransactionRepository transactionRepository;

  @Override
  public ResponseCustom bookingRoom(BookingRequest bookingRequest, UUID userId,String ipAddr)
      throws ParseException {
    LocalDate checkInDate = bookingRequest.getCheckInDate();
    LocalDate checkOutDate = bookingRequest.getCheckOutDate();
    if (checkInDate.isAfter(checkOutDate)) {
      throw new BadRequestException(ErrorMessage.CHECK_IN_CANT_AFTER_CHECK_OUT);
    }
    if (checkInDate.equals(checkOutDate)) {
      throw new BadRequestException(ErrorMessage.CHECK_IN_CANT_EQUAL_CHECK_OUT);
    }
    if (bookingRepository.checkExist(bookingRequest.getHomeId(), checkInDate, checkOutDate)) {
      throw new BadRequestException(ErrorMessage.BOOKING_DATE_NOT_AVAILABLE);
    }
    Home home =
        homeRepository
            .findById(bookingRequest.getHomeId())
            .orElseThrow(() -> new NotFoundException(ErrorMessage.HOME_NOT_EXISTS));

   ;
    BigDecimal pricePerDay = BigDecimal.valueOf(home.getPrice());
    long day =
            DAYS.between(checkInDate, checkOutDate);
    BigDecimal priceForStay = pricePerDay.multiply(BigDecimal.valueOf(day));
    Booking booking = new Booking();
    booking.setHomeId(home.getId());
    booking.setNumOfAdults(bookingRequest.getNumOfAdults());
    booking.setNumOfBabies(bookingRequest.getNumOfBabies());
    booking.setNumOfChildren(bookingRequest.getNumOfChildren());
    booking.setNumOfPets(bookingRequest.getNumOfPets());
    booking.setPricePerDay(pricePerDay);
    booking.setPriceForStay(priceForStay);
    booking.setUserId(userId);
    booking.setCheckInDate(bookingRequest.getCheckInDate());
    booking.setCheckOutDate(bookingRequest.getCheckOutDate());
    booking.setCreatedAt(new Timestamp(new Date().getTime()));
    booking.setUpdatedAt(new Timestamp(new Date().getTime()));
    booking.setStatus(BookingStatus.WAITING);
    booking.setRating(RatingStatus.NOT_YET_RATED);

    PaymentRequest paymentRequest = new PaymentRequest();
    paymentRequest.setAmount(booking.getPriceForStay());
    String orderInfo = String.format("%s - %d DAY ", CommonFunction.toEngChar(home.getName()),day);
    paymentRequest.setVnpOrderInfo(orderInfo);
    paymentRequest.setLanguage("vn");
    paymentRequest.setIpAddr(ipAddr);
    paymentRequest.setReturnURL(bookingRequest.getReturnURL());
    Transactions transaction = null;
    try {
      transaction = paymentService.makePayment(paymentRequest);
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
    booking.setTransactions(transaction);
    Booking inserted = bookingRepository.save(booking);
    return ResponseCustom.successResponse(bookingModelToBookingResponse(inserted, true));
//    return  ResponseCustom.successResponse();
  }

  @Override
  public ResponseCustom getListBookingByClient(UUID userId, String status, int page, int paging) {
    Pageable pageable = PageRequest.of(page - 1, paging);
    Page<BookingDTO> bookings = bookingRepository.getBookingByClient(userId, status, pageable);

    PageInfo pageInfo =
        new PageInfo(
            pageable.getPageNumber() + 1,
            bookings.getTotalPages(),
            bookings.getTotalElements());
    return ResponseCustom.successResponseWithMeta(toListBookingResponses(bookings.toList(), true),pageInfo);
  }

  @Override
  public ResponseCustom acceptBooking(UUID userId, UUID bookingId) {
    BookingDTO bookingDTO =
        bookingRepository
            .getBookingDetailByOwner(userId.toString(), bookingId)
            .orElseThrow(
                () -> new NotFoundException(ErrorMessage.BOOKING_NOT_FOUND_OR_ACCESS_DENIED));
    Booking booking = bookingRepository.findById(bookingId).get();
    if (!booking.getStatus().equals(BookingStatus.WAITING)) {
      throw new BadRequestException(ErrorMessage.FAIL_TO_ACCEPT_BOOKING);
    }
    booking.setStatus(BookingStatus.ACCEPTED);
    booking.setRating(RatingStatus.NOT_YET_RATED);
    bookingRepository.save(booking);
    BookingResponse bookingResponse = toBookingResponse(bookingDTO, false);
    bookingResponse.setStatus(booking.getStatus());
    return ResponseCustom.successResponse(bookingResponse);
  }

  @Override
  public ResponseCustom rejectBooking(
      UUID userId, UUID bookingId, CancelBookingRequest cancelBookingRequest) {
    BookingDTO bookingDTO =
        bookingRepository
            .getBookingDetailByOwner(userId.toString(), bookingId)
            .orElseThrow(
                () -> new NotFoundException(ErrorMessage.BOOKING_NOT_FOUND_OR_ACCESS_DENIED));
    Booking booking = bookingRepository.findById(bookingId).get();
    if (!booking.getStatus().equals(BookingStatus.WAITING)) {
      throw new BadRequestException(ErrorMessage.FAIL_TO_REJECT_BOOKING);
    }
    booking.setStatus(BookingStatus.REJECT);
    booking.setReason(cancelBookingRequest.getReason());
    bookingRepository.save(booking);
    BookingResponse bookingResponse = toBookingResponse(bookingDTO, false);
    bookingResponse.setStatus(booking.getStatus());
    return ResponseCustom.successResponse(bookingResponse);
  }

  @Override
  public ResponseCustom cancelBooking(
      UUID userId, UUID bookingId, CancelBookingRequest cancelBookingRequest) {
    BookingDTO bookingDTO =
        bookingRepository
            .getBookingDetailByClient(userId, bookingId)
            .orElseThrow(
                () -> new NotFoundException(ErrorMessage.BOOKING_NOT_FOUND_OR_ACCESS_DENIED));
    Booking booking = bookingRepository.findById(bookingId).get();
    if (!booking.getStatus().equals(BookingStatus.WAITING)) {
      throw new BadRequestException(ErrorMessage.FAIL_TO_CANCEL_BOOKING);
    }
    booking.setStatus(BookingStatus.CANCEL);
    booking.setReason(cancelBookingRequest.getReason());
    booking.setCancelDate(new Timestamp(new Date().getTime()));
    bookingRepository.save(booking);
    BookingResponse bookingResponse = toBookingResponse(bookingDTO, false);
    bookingResponse.setStatus(booking.getStatus());
    return ResponseCustom.successResponse(bookingResponse);
  }

  @Override
  public ResponseCustom getBookedDate(UUID homeId) {

    homeRepository
        .findById(homeId)
        .orElseThrow(() -> new NotFoundException(ErrorMessage.HOME_NOT_EXISTS));
    return ResponseCustom.successResponse(bookingRepository.getBookedDate(homeId));
  }

  @Override
  public ResponseCustom getListBookingByOwner(UUID userId, String status, int page, int paging) {
    Pageable pageable = PageRequest.of(page - 1, paging);

    Page<BookingDTO> bookings =
        bookingRepository.getListBookingByOwner(userId.toString(), status, pageable);
    PageInfo pageInfo =
        new PageInfo(
            pageable.getPageNumber() + 1,
            bookings.getTotalPages(),
            bookings.getTotalElements());

    return ResponseCustom.successResponseWithMeta(toListBookingResponses(bookings.toList(), false),pageInfo);
  }

  @Override
  public ResponseCustom getBookingDetailByClient(UUID userId, UUID bookingId) {
    BookingDTO bookingDTO =
        bookingRepository
            .getBookingDetailByClient(userId, bookingId)
            .orElseThrow(
                () -> new NotFoundException(ErrorMessage.BOOKING_NOT_FOUND_OR_ACCESS_DENIED));
    return ResponseCustom.successResponse(toBookingResponse(bookingDTO, true));
  }

  @Override
  public ResponseCustom getBookingDetailByOwner(UUID userId, UUID bookingId) {
    BookingDTO bookingDTO =
        bookingRepository
            .getBookingDetailByOwner(userId.toString(), bookingId)
            .orElseThrow(
                () -> new NotFoundException(ErrorMessage.BOOKING_NOT_FOUND_OR_ACCESS_DENIED));
    return ResponseCustom.successResponse(toBookingResponse(bookingDTO, false));
  }

  private List<BookingResponse> toListBookingResponses(
      List<BookingDTO> bookingList, boolean isClient) {
    List<BookingResponse> bookingResponseList = new ArrayList<>();
    for (BookingDTO bookingDTO : bookingList) {
      bookingResponseList.add(toBookingResponse(bookingDTO, isClient));
    }
    return bookingResponseList;
  }

  private BookingResponse toBookingResponse(BookingDTO bookingDTO, boolean isClient) {
    BookingResponse bookingResponse = new BookingResponse();
    bookingResponse.setId(UUID.fromString(bookingDTO.getId()));
    bookingResponse.setCancelDate(bookingDTO.getCancelDate());
    bookingResponse.setCheckInDate(bookingDTO.getCheckInDate());
    bookingResponse.setCheckOutDate(bookingDTO.getCheckOutDate());
    bookingResponse.setCreatedAt(bookingDTO.getCreatedAt());
    bookingResponse.setUpdatedAt(bookingDTO.getUpdatedAt());
    bookingResponse.setPriceForStay(bookingDTO.getPriceForStay());
    bookingResponse.setPricePerDay(bookingDTO.getPricePerDay());
    bookingResponse.setStatus(bookingDTO.getStatus());
    bookingResponse.setNumOfAdults(bookingDTO.getNumOfAdults());
    bookingResponse.setNumOfBabies(bookingDTO.getNumOfBabies());
    bookingResponse.setNumOfChildren(bookingDTO.getNumOfChildren());
    bookingResponse.setRating(bookingDTO.getRating().name());

    Home home = homeRepository.findById(UUID.fromString(bookingDTO.getHomeId()))
            .orElseThrow(() -> new BadRequestException(ErrorMessage.HOME_NOT_EXISTS));
    bookingResponse.setHome(new HomeResponse(home));

    Transactions transactions =
        transactionRepository
            .findById(UUID.fromString(bookingDTO.getTransactionId()))
            .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.TRANSACTION_NOT_FOUND));
    TransactionsResponse transactionsResponse = new TransactionsResponse();
    transactionsResponse.setAmount(transactions.getAmount());
    transactionsResponse.setBankTranNo(transactions.getBankTranNo());
    transactionsResponse.setTimeOver(transactions.getTimeOver());
    transactionsResponse.setCreatedAt(transactions.getCreatedAt());
    transactionsResponse.setUpdatedAt(transactions.getUpdatedAt());
    transactionsResponse.setPaymentUrl(transactions.getPaymentURL());
    transactionsResponse.setTxtRef(transactions.getTxtRef());
    transactionsResponse.setStatus(transactions.getStatus());

    bookingResponse.setTransaction(transactionsResponse);

    if (!isClient) {
      UserInfo userInfo = (UserInfo) authClientService.getUserByIdUserParam(bookingDTO.getUserId());
      bookingResponse.setUserInfo(userInfo);
    }
    return bookingResponse;
  }

  private BookingResponse bookingModelToBookingResponse(Booking booking, boolean isClient) {
    BookingResponse bookingResponse = new BookingResponse();
    bookingResponse.setId(booking.getId());
    bookingResponse.setCancelDate(booking.getCancelDate());
    bookingResponse.setCheckInDate(booking.getCheckInDate());
    bookingResponse.setCheckOutDate(booking.getCheckOutDate());
    bookingResponse.setNumOfAdults(booking.getNumOfAdults());
    bookingResponse.setNumOfChildren(booking.getNumOfChildren());
    bookingResponse.setNumOfBabies(booking.getNumOfBabies());
    bookingResponse.setNumOfPets(booking.getNumOfPets());
    bookingResponse.setCreatedAt(booking.getCreatedAt());
    bookingResponse.setUpdatedAt(booking.getUpdatedAt());
    bookingResponse.setPriceForStay(booking.getPriceForStay());
    bookingResponse.setPricePerDay(booking.getPricePerDay());
    bookingResponse.setStatus(booking.getStatus());
    Home home = homeRepository.findById(booking.getHomeId())
            .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.HOME_NOT_EXISTS));
    bookingResponse.setHome(new HomeResponse(home));

    Transactions transactions = booking.getTransactions();
    TransactionsResponse transactionsResponse = new TransactionsResponse();
    transactionsResponse.setAmount(transactions.getAmount());
    transactionsResponse.setBankTranNo(transactions.getBankTranNo());
    transactionsResponse.setTimeOver(transactions.getTimeOver());
    transactionsResponse.setCreatedAt(transactions.getCreatedAt());
    transactionsResponse.setUpdatedAt(transactions.getUpdatedAt());
    transactionsResponse.setPaymentUrl(transactions.getPaymentURL());
    transactionsResponse.setTxtRef(transactions.getTxtRef());
    transactionsResponse.setStatus(transactions.getStatus());

    bookingResponse.setTransaction(transactionsResponse);

    if (!isClient) {
      UserInfo userInfo = authClientService.getUserByIdUserParam(booking.getUserId().toString());
      bookingResponse.setUserInfo(userInfo);
    }
    return bookingResponse;
  }
}
