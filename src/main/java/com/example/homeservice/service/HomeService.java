package com.example.homeservice.service;

import com.example.homeservice.payload.request.HomeRequest;

import com.example.homeservice.payload.response.HomeDetailResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

public interface HomeService {
    public ResponseEntity getAll(int page, int paging,String orderBy);

    public ResponseEntity getHome(UUID id);

    public
    HomeDetailResponse createHome(String idUser, HomeRequest homeRequest);

    public ResponseEntity getHomeByIdUser(String idUser, int page, int paging,String orderBy);

    public ResponseEntity getCategories();

    public ResponseEntity getBathroom();

    public ResponseEntity getBedroomAndLaundry();

    public ResponseEntity getEntertainment();
    public ResponseEntity getFamily();
    public ResponseEntity getHeatingAndCooling();
    public ResponseEntity getHomeSafety();
    public ResponseEntity getInternetAndOffice();
    public ResponseEntity getKitchenAndDining();
    public ResponseEntity getLocationFeature();
    public ResponseEntity getOutdoor();
    public ResponseEntity getParkingAndFacilities();
    public ResponseEntity getServices();

    public ResponseEntity getHome(int category, int page, int paging,String orderBy);

    public ResponseEntity uploadImage(List<MultipartFile> files);

    public ResponseEntity getHomeWaitingApprove(int page, int paging);

    public ResponseEntity confirmHome(UUID idHome, String status);

    public ResponseEntity editHome(UUID idHome, HomeRequest homeRequest);
}
