package com.example.homeservice.repository;

import com.example.homeservice.domain.model.BedroomAndLaundry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BedroomAndLaundryRepository extends JpaRepository<BedroomAndLaundry, Integer> {
    BedroomAndLaundry findByTitle(String title);

    Boolean existsByTitle(String title);
}
