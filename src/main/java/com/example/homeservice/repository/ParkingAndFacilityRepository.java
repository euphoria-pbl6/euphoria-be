package com.example.homeservice.repository;

import com.example.homeservice.domain.model.ParkingAndFacilities;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParkingAndFacilityRepository extends JpaRepository<ParkingAndFacilities, Integer> {
    ParkingAndFacilities findByTitle(String title);

    Boolean existsByTitle(String title);
}
