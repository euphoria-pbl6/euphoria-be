package com.example.homeservice.repository;

import com.example.homeservice.domain.model.Outdoor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OutdoorRepository extends JpaRepository<Outdoor, Integer> {
    Outdoor findByTitle(String title);

    Boolean existsByTitle(String title);
}
