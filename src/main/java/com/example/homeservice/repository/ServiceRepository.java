package com.example.homeservice.repository;

import com.example.homeservice.domain.model.Services;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<Services, Integer> {
    Services findByTitle(String title);

    Boolean existsByTitle(String title);
}
