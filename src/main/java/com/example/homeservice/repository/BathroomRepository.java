package com.example.homeservice.repository;

import com.example.homeservice.domain.model.Bathroom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BathroomRepository extends JpaRepository<Bathroom, Integer> {
    Bathroom findByTitle(String title);

    Boolean existsByTitle(String title);
}
