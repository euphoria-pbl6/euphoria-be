package com.example.homeservice.repository;

import com.example.homeservice.domain.model.Entertainment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntertainmentRepository extends JpaRepository<Entertainment, Integer> {
    Entertainment findByTitle(String title);

    Boolean existsByTitle(String title);
}
