package com.example.homeservice.repository;

import com.example.homeservice.domain.model.InternetAndOffice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InternetAndOfficeRepository extends JpaRepository<InternetAndOffice, Integer> {
    InternetAndOffice findByTitle(String title);

    Boolean existsByTitle(String title);
}
