package com.example.homeservice.repository.booking;

import com.example.homeservice.domain.model.booking.Booking;
import com.example.homeservice.payload.dto.BookingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface BookingRepository extends JpaRepository<Booking, UUID> {
    @Query(
            value = "SELECT  " +
                    "    EXISTS (  " +
                    "    SELECT  " +
                    "        *  " +
                    "    FROM  " +
                    "        booking b  " +
                    "    WHERE  " +
                    "        home_id = :home_id  and  " +
                    "    :start < TO_CHAR(b.check_out_date, 'YYYY-MM-DD')  " +
                    "        AND    " +
                    "    :end >= TO_CHAR(b.check_in_date , 'YYYY-MM-DD')   " +
                    ")",
            nativeQuery = true
    )
    boolean checkExist(@Param("home_id") UUID homeId, LocalDate start, LocalDate end);

  @Query(
      value =
          "SELECT  "
              + "    cast(b.id AS varchar) AS id,  "
              + "    b.cancel_date AS cancelDate,  "
              + "    b.check_in_date AS checkInDate,  "
              + "    b.check_out_date AS checkOutDate,  "
              + "    b.num_of_children AS numOfChildren,  "
              + "    b.num_of_babies AS numOfBabies,  "
              + "    b.num_of_adults AS numOfAdults,  "
              + "    b.num_of_pets AS numOfPets,  "
              + "    b.created_at AS createdAt,  "
              + "    b.updated_at AS updatedAt,  "
              + "    cast(b.home_id AS varchar) AS homeId ,  "
              + "    cast(b.user_id AS varchar) AS userId, "
              + "    cast(b.transactions_id as varchar) as transactionId, "
              + "    b.is_refund AS isRefund,  "
              + "    b.price_for_stay AS priceForStay,  "
              + "    b.price_per_day AS pricePerDay,  "
              + "    b.status, "
              + "    b.rating "
              + "FROM  "
              + "   booking b  "
              + "WHERE  "
              + "   user_id = :user_id  "
              + "   and status like :status",
      nativeQuery = true)
  Page<BookingDTO> getBookingByClient(
      @Param("user_id") UUID userId, String status, Pageable pageable);

    @Query(
            value = "select  " +
                    "   to_char(date_generate.data, 'YYYY-MM-DD') as date " +
                    "from  " +
                    "   (  " +
                    "   select  " +
                    "      generate_series(       " +
                    "          CURRENT_DATE,       " +
                    "            (select max(check_out_date) from booking WHERE home_id = :home_id and status != 'CANCEL'),       " +
                    "            'P1D' \\:\\: interval       " +
                    "         ) as data  " +
                    "         ) as date_generate  " +
                    "inner join booking b  " +
                    "         on  " +
                    "   to_char(date_generate.data, 'YYYY-MM-DD') BETWEEN to_char(b.check_in_date , 'YYYY-MM-DD') and to_char(b.check_out_date -'P1D' \\:\\:INTERVAL, 'YYYY-MM-DD')  " +
                    "where  " +
                    "   b.home_id = :home_id  " +
                    "order by  " +
                    "   date",
            nativeQuery = true
    )
    List<String> getBookedDate(
            @Param("home_id") UUID homeId);

  @Query(
      value =
          "SELECT  "
              + "    cast(b.id AS varchar) AS id,  "
              + "    b.cancel_date AS cancelDate,  "
              + "    b.check_in_date AS checkInDate,  "
              + "    b.check_out_date AS checkOutDate,  "
              + "    b.num_of_children AS numOfChildren,  "
              + "    b.num_of_babies AS numOfBabies,  "
              + "    b.num_of_adults AS numOfAdults,  "
              + "    b.num_of_pets AS numOfPets,  "
              + "    b.created_at AS createdAt,  "
              + "    b.updated_at AS updatedAt,  "
              + "    cast(b.home_id AS varchar) AS homeId ,  "
              + "    cast(b.user_id AS varchar) AS userId, "
              + "    cast(b.transactions_id as varchar) as transactionId, "
              + "    b.is_refund AS isRefund,  "
              + "    b.price_for_stay AS priceForStay,  "
              + "    b.price_per_day AS pricePerDay,  "
              + "    b.status, "
              + "    b.rating "
              + "FROM    "
              + "   home h   "
              + "JOIN booking b    "
              + "ON   "
              + "   h.id = b.home_id   "
              + "WHERE   "
              + "   h.id_user = :user_id and status like :status   "
              + "ORDER BY b.updated_at  ",
      nativeQuery = true)
  Page<BookingDTO> getListBookingByOwner(
      @Param("user_id") String userId, @Param("status") String status, Pageable pageable);

  @Query(
      value =
          "SELECT  "
              + "    cast(b.id AS varchar) AS id,  "
              + "    b.cancel_date AS cancelDate,  "
              + "    b.check_in_date AS checkInDate,  "
              + "    b.check_out_date AS checkOutDate,  "
              + "    b.num_of_children AS numOfChildren,  "
              + "    b.num_of_babies AS numOfBabies,  "
              + "    b.num_of_adults AS numOfAdults,  "
              + "    b.num_of_pets AS numOfPets,  "
              + "    b.created_at AS createdAt,  "
              + "    b.updated_at AS updatedAt,  "
              + "    cast(b.home_id AS varchar) AS homeId ,  "
              + "    cast(b.user_id AS varchar) AS userId, "
              + "    cast(b.transactions_id as varchar) as transactionId, "
              + "    b.is_refund AS isRefund,  "
              + "    b.price_for_stay AS priceForStay,  "
              + "    b.price_per_day AS pricePerDay,  "
              + "    b.status, "
              + "    b.rating "
              + "FROM    "
              + "   home h   "
              + "JOIN booking b    "
              + "ON   "
              + "   h.id = b.home_id   "
              + "WHERE   "
              + "   h.id_user = :user_id and b.id = :booking_id ",
      nativeQuery = true)
  Optional<BookingDTO> getBookingDetailByOwner(
      @Param("user_id") String userId, @Param("booking_id") UUID bookingId);

  @Query(
      value =
          "SELECT  "
              + "    cast(b.id AS varchar) AS id,  "
              + "    b.cancel_date AS cancelDate,  "
              + "    b.check_in_date AS checkInDate,  "
              + "    b.check_out_date AS checkOutDate,  "
              + "    b.num_of_children AS numOfChildren,  "
              + "    b.num_of_babies AS numOfBabies,  "
              + "    b.num_of_adults AS numOfAdults,  "
              + "    b.num_of_pets AS numOfPets,  "
              + "    b.created_at AS createdAt,  "
              + "    b.updated_at AS updatedAt,  "
              + "    cast(b.home_id AS varchar) AS homeId ,  "
              + "    cast(b.user_id AS varchar) AS userId, "
              + "    cast(b.transactions_id as varchar) as transactionId, "
              + "    b.is_refund AS isRefund,  "
              + "    b.price_for_stay AS priceForStay,  "
              + "    b.price_per_day AS pricePerDay,  "
              + "    b.status, "
              + "    b.rating "
              + "FROM booking b "
              + "WHERE   "
              + "   b.user_id  = :user_id and b.id = :booking_id ",
      nativeQuery = true)
  Optional<BookingDTO> getBookingDetailByClient(
      @Param("user_id") UUID userId, @Param("booking_id") UUID bookingId);

    @Query(value = "SELECT b FROM Booking b " +
            "WHERE b.homeId = :idHome AND b.status = :status")
    List<Booking> getBookingByHome(@Param("idHome") UUID idHome, @Param("status") String status);

  @Query(
      value =
          "SELECT  "
              + "    cast(b.id AS varchar) AS id,  "
              + "    b.cancel_date AS cancelDate,  "
              + "    b.check_in_date AS checkInDate,  "
              + "    b.check_out_date AS checkOutDate,  "
              + "    b.num_of_children AS numOfChildren,  "
              + "    b.num_of_babies AS numOfBabies,  "
              + "    b.num_of_adults AS numOfAdults,  "
              + "    b.num_of_pets AS numOfPets,  "
              + "    b.created_at AS createdAt,  "
              + "    b.updated_at AS updatedAt,  "
              + "    cast(b.home_id AS varchar) AS homeId ,  "
              + "    cast(b.user_id AS varchar) AS userId, "
              + "    cast(b.transactions_id as varchar) as transactionId, "
              + "    b.is_refund AS isRefund,  "
              + "    b.price_for_stay AS priceForStay,  "
              + "    b.price_per_day AS pricePerDay,  "
              + "    b.status, "
              + "    b.rating "
              + "FROM   "
              + "booking b "
              + "WHERE   "
              + "   b.user_id  = :user_id and b.id = :booking_id and b.rating = :rating",
      nativeQuery = true)
  Optional<BookingDTO> getBookingDetailByClient(
      @Param("user_id") UUID userId,
      @Param("booking_id") UUID bookingId,
      @Param("rating") String rating);

  @Query(
      value =
          "SELECT  "
              + "    cast(b.id AS varchar) AS id,  "
              + "    b.cancel_date AS cancelDate,  "
              + "    b.check_in_date AS checkInDate,  "
              + "    b.check_out_date AS checkOutDate,  "
              + "    b.num_of_children AS numOfChildren,  "
              + "    b.num_of_babies AS numOfBabies,  "
              + "    b.num_of_adults AS numOfAdults,  "
              + "    b.num_of_pets AS numOfPets,  "
              + "    b.created_at AS createdAt,  "
              + "    b.updated_at AS updatedAt,  "
              + "    cast(b.home_id AS varchar) AS homeId ,  "
              + "    cast(b.user_id AS varchar) AS userId, "
              + "    cast(b.transactions_id as varchar) as transactionId, "
              + "    b.is_refund AS isRefund,  "
              + "    b.price_for_stay AS priceForStay,  "
              + "    b.price_per_day AS pricePerDay,  "
              + "    b.status, "
              + "    b.rating "
              + "FROM  "
              + "    home h  "
              + "JOIN booking b     "
              + "ON  "
              + "    h.id = b.home_id "
              + "WHERE "
              + "    h.id_user = :user_id "
              + "    AND b.rating = :rating "
              + "ORDER BY "
              + "    b.updated_at",
      nativeQuery = true)
  List<BookingDTO> getBookingByRating(
      @Param("user_id") String userId, @Param("rating") String rating);

    Booking getBookingByTransactionsId(UUID id);
}
