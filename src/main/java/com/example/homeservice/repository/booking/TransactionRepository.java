package com.example.homeservice.repository.booking;

import com.example.homeservice.domain.model.booking.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.UUID;
@Repository
public interface TransactionRepository extends JpaRepository<Transactions, UUID> {
    @Query(
            value = "SELECT * FROM transactions t  WHERE txt_ref = :txt_ref AND amount = :amount AND status = 'PENDING' LIMIT 1",
            nativeQuery = true
    )
    Transactions getTransactionsByTxtRefAndCreatedAt(@Param("txt_ref")String txtRef,@Param("amount") BigDecimal amount);
}
