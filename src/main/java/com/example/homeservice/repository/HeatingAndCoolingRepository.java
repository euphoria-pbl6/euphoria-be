package com.example.homeservice.repository;

import com.example.homeservice.domain.model.HeatingAndCooling;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeatingAndCoolingRepository extends JpaRepository<HeatingAndCooling, Integer> {
    HeatingAndCooling findByTitle(String title);

    Boolean existsByTitle(String title);
}
