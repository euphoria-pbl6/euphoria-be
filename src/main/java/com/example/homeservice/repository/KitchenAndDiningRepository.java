package com.example.homeservice.repository;

import com.example.homeservice.domain.model.KitchenAndDining;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KitchenAndDiningRepository extends JpaRepository<KitchenAndDining, Integer> {
    KitchenAndDining findByTitle(String title);

    Boolean existsByTitle(String title);
}
