package com.example.homeservice.repository;

import com.example.homeservice.domain.model.LocationFeatures;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationFeatureRepository extends JpaRepository<LocationFeatures, Integer>{
    LocationFeatures findByTitle(String title);

    Boolean existsByTitle(String title);
}
