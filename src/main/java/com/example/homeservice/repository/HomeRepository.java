package com.example.homeservice.repository;

import com.example.homeservice.domain.model.EStatus;
import com.example.homeservice.domain.model.Home;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface HomeRepository extends JpaRepository<Home, UUID> {
    @Override
    Optional<Home> findById(UUID id);

    Optional<Page<Home>> findHomeByIdUser(String id, Pageable pageable);

    @Query(value = "SELECT h FROM Home h WHERE h.category.id = :idCategory AND h.status.name = :status")
    Page<Home> findHomeByCategory(@Param("idCategory") int idCategory,
                                  @Param("status") EStatus status,
                                  Pageable pageable);

    @Query(value = "SELECT h FROM Home h WHERE h.status.name = :status")
    Page<Home> findAllHome(@Param("status") EStatus status,
                                  Pageable pageable);

    @Query(value = "SELECT h FROM Home h WHERE h.status.id = :status")
    Page<Home> findHomeByStatusId(@Param("status") int status, Pageable pageable);
}
