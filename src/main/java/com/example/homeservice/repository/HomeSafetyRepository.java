package com.example.homeservice.repository;

import com.example.homeservice.domain.model.HomeSafety;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeSafetyRepository extends JpaRepository<HomeSafety, Integer> {
    HomeSafety findByTitle(String title);

    Boolean existsByTitle(String title);
}
