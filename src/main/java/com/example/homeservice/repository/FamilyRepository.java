package com.example.homeservice.repository;

import com.example.homeservice.domain.model.Family;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyRepository extends JpaRepository<Family, Integer> {
    Family findByTitle(String title);

    Boolean existsByTitle(String title);
}
