package com.example.homeservice.repository;

import com.example.homeservice.domain.model.EStatus;
import com.example.homeservice.domain.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Integer> {
    Status findById(int id);

    Status findByName(EStatus name);
}
