package com.example.homeservice.repository.rating;

import com.example.homeservice.domain.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RatingRepository extends JpaRepository<Rating, UUID> {
    @Override
    Optional<Rating> findById(UUID uuid);

    @Query(value = "SELECT r FROM Rating r WHERE r.booking.homeId = :idHome")
    List<Rating> findRatingByHome(@Param("idHome") UUID idHome);

    @Query(value = "SELECT r FROM Rating r WHERE r.booking.id = :idBooking")
    Optional<Rating> findByBooking(@Param("idBooking") UUID idBooking);
}
